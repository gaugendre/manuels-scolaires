# Generated by Django 2.2 on 2019-04-06 17:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("manuels", "0029_auto_20190406_1904"),
    ]

    operations = [
        migrations.RenameField(
            model_name="suppliesrequirement",
            old_name="fields",
            new_name="field",
        ),
    ]
