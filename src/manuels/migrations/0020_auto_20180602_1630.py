# Generated by Django 2.0.5 on 2018-06-02 14:30

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("manuels", "0019_auto_20180531_0815"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="teacher",
            options={
                "verbose_name": "coordonnateur",
                "verbose_name_plural": "coordonnateurs",
            },
        ),
        migrations.AlterField(
            model_name="book",
            name="teacher",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="manuels.Teacher",
                verbose_name="coordonnateur",
            ),
        ),
        migrations.AlterField(
            model_name="suppliesrequirement",
            name="teacher",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="manuels.Teacher",
                verbose_name="coordonnateur",
            ),
        ),
    ]
