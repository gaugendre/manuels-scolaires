# Generated by Django 2.0.5 on 2018-05-22 07:34

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("manuels", "0004_auto_20180522_0148"),
    ]

    operations = [
        migrations.CreateModel(
            name="SuppliesRequirement",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(auto_now_add=True, verbose_name="créé le"),
                ),
                (
                    "updated_at",
                    models.DateTimeField(auto_now=True, verbose_name="mis à jour le"),
                ),
                ("supplies", models.TextField(verbose_name="fournitures")),
                (
                    "level",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="manuels.Level",
                        verbose_name="classe",
                    ),
                ),
                (
                    "teacher",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="manuels.Teacher",
                        verbose_name="enseignant",
                    ),
                ),
            ],
            options={
                "verbose_name": "demande de fournitures",
                "verbose_name_plural": "demandes de fournitures",
            },
        ),
    ]
