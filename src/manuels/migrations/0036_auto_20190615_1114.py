# Generated by Django 2.2.1 on 2019-06-15 09:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("manuels", "0035_commonsupply_order"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="commonsupply",
            options={
                "ordering": ("order",),
                "verbose_name": "fourniture commune",
                "verbose_name_plural": "fournitures communes",
            },
        ),
    ]
